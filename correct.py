#!/usr/bin/python

# Usage: python correct.py INPUT OUTPUT LOG_FILE

import sys
import math
from pathlib import Path

def split_csv(line):
  values = line.strip().split(';')
  return [v.strip() for v in values]

def log(msg, log_file):
  print(msg)
  log_file.write(msg + "\n")

args = sys.argv
command = args.pop(0)
if len(args) != 3:
  print(f'Usage: python {command} INPUT OUTPUT LOG_FILE')
  sys.exit(1)

in_file = Path(args[0])
out_file = Path(args[1])
log_file = Path(args[2])

# Input validation.
if not in_file.is_file():
  print(f'Error: no such file: {in_file}')
  sys.exit(1)

if out_file.is_file():
  print(f'Error: file already exists: {out_file}')
  sys.exit(1)

if log_file.is_file():
  print(f'Error: file already exists: {log_file}')
  sys.exit(1)

if args[1] == args[2]:
  print("Error: OUTPUT and LOG_FILE can't be the same file.")
  sys.exit(1)

with in_file.open('r') as in_file_handle:
  in_lines = in_file_handle.readlines()
in_lines = [line for line in in_lines if ';' in line]

out_lines = [in_lines[0]]

header = split_csv(in_lines.pop(0))
num_columns = len(header)
num_ballots = len(in_lines)

stats = {
  'OK': 0,
  'Corrected': 0,
  'Invalid': 0,
}

with log_file.open('w') as log_file:
  log(f'Processing {num_ballots} ballots …', log_file)

  for line in in_lines:
    values = split_csv(line)
    label = values[0]
    errors = []

    if len(values) != num_columns:
      values = []
      errors.append(f'Ballot had wrong number of entries: {len(values)}.')

    positions = {}
    duplicates = {}
    for i in range(1, len(values)):
      pos = values[i]
      if pos == '':
        continue
      pos = int(pos)
      if pos in positions:
        if not pos in duplicates:
          duplicates[pos] = [positions[pos]]
        duplicates[pos].append(i)
        values[i] = ''
      else:
        positions[pos] = i

    for pos in duplicates:
      errors.append(f'Duplicate number: {pos}. Deleted all such entries.')
      del positions[pos]
      for i in duplicates[pos]:
        values[i] = ''

    pos_list_sorted = list(positions.keys())
    pos_list_sorted.sort()
    reordered = False
    last_pos = 0
    for pos in pos_list_sorted:
      if pos != last_pos + 1:
        reordered = True
        values[positions[pos]] = str(last_pos + 1)
        pos = last_pos + 1
      last_pos = pos
    if reordered:
      errors.append(f'Corrected numbering. Was: {", ".join([str(pos) for pos in pos_list_sorted])}.')

    if positions == {}:
      new_line = False
      stats['Invalid'] += 1
      errors.append('Ballot empty. Deleted.')
    else:
      new_line = ';'.join(values) + "\n"
      out_lines.append(new_line)

    if errors != []:
      log('  ' + label + ':', log_file)
      for msg in errors:
        log('    ' + msg, log_file)
      log('    Was: ' + line.strip(), log_file)
      if new_line:
        stats['Corrected'] += 1
        log('    New: ' + new_line.strip(), log_file)
    else:
      stats['OK'] += 1

  with out_file.open('w') as out_file_handle:
    out_file_handle.write(''.join(out_lines))

  log('Processing finished.', log_file)
  for stat in stats:
    log(' ' * (9 - len(stat)) + stat + ': ' + str(stats[stat]), log_file)
