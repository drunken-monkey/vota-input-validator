#!/bin/bash

RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
NC=`tput sgr0`

TEST_OUTPUT_DIR=/tmp/correct.py-tests

# Make sure directory is empty.
rm -rf "$TEST_OUTPUT_DIR"
mkdir -p "$TEST_OUTPUT_DIR"

for INPUT_FILE in tests/original/* ; do
  BASE=`basename "${INPUT_FILE%.csv}"`
  echo "${YELLOW}→${NC} Checking $BASE …"
  OUT_FILE="$TEST_OUTPUT_DIR/$BASE.csv"
  LOG_FILE="$TEST_OUTPUT_DIR/$BASE.txt"
  python correct.py "$INPUT_FILE" "$OUT_FILE" "$LOG_FILE" >/dev/null
  if diff --color "tests/processed/$BASE.csv" "$OUT_FILE" ; then
    echo "  ${GREEN}✔${NC} Processed correctly."
  else
    echo "  ${RED}✖${NC} Not processed correctly (see above)."
  fi
  if diff --color "tests/logs/$BASE.txt" "$LOG_FILE" ; then
    echo "  ${GREEN}✔${NC} Logs are correct."
  else
    echo "  ${RED}✖${NC} Logs are incorrect (see above)."
  fi
  echo
done
